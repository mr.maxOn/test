import React from 'react'
import { Route, Routes } from 'react-router-dom';
import Home from './Home.jsx'
import Login from './Login.jsx'
import Signup from './Signup.jsx'

function Layout() {
   return (
    <div>
      <Routes>
        <Route path='/' element={<Home/>} />
        <Route path='/login' element={<Login/>} />
        <Route path='/signup' element={<Signup/>} />
      </Routes>
    </div>
  )
}
export default Layout;