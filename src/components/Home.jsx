import React from 'react'
import { NavLink } from 'react-router-dom';
import app from '../firebaseCfg';

function Home() {
   return (
    <div>
      <NavLink to='/login'>Login</NavLink>
      <NavLink to='/signup'>Signup</NavLink>
      <button onClick={()=> app.auth().signOut()}>Sign out</button>
    </div>
  )
}
export default Home;